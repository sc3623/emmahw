# Assignment Home #

Homework assignments will be added to this repository. 

### Getting assignments ###

* Please fork this repository to your own private repository named `userIDhw` in the Homework project.
* I will ensure that only you and I have access to your repository
* Each assignment will be added in a directory named such as hw0, hw1, etc.

### Using git ###

While working on the assignment, use git to save your progress.

Within your repository, use git `add/commit/push` commands regularly to save work.

* Add the file: `$ git add userID.#.ipynb`
* Commit the staged files: `$ git commit -m 'initial commit of hw0'`
* Ensure up to date with remote:`$ git pull origin master`
* Push to remote: `$ git push origin master`

### Submitting assignments ###
When you have completed your assignment, please add a tag to show that it is the final product.

The timestap of the tag being pushed will be the submission time.

* Add the tag: `$ git tag -a hw0 -m 'hw0 submission'`
* Push the tag: `$ git push origin hw0` 

If changes are required, remove the tag and apply it to the new commited files.

* Remove the tag: `$ git tag -d hw0`
* Push removal of tag: `$ git push origin :hw0`

Then repeat above steps to tag the modified files.